# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from account.models import Department, Course, Account

admin.site.register(Department)
admin.site.register(Course)
admin.site.register(Account)