# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.


def login(request):

    context = {
    }
    return render(request, 'login.html', context)


def register(request):
    context = {

    }
    return render(request, 'register.html', context)
