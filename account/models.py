# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.core.urlresolvers import reverse


class Department(models.Model):
    name = models.CharField(max_length=100)
    abbr = models.CharField(max_length=40)

    def __str__(self):
        return self.name


class Course(models.Model):
    department = models.ForeignKey(Department)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Account(models.Model):
    reg_no = models.CharField(max_length=100)
    mobile = models.CharField(max_length=100)
    department = models.ForeignKey(Department)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    course = models.ForeignKey(Course)

    def __str__(self):
        return self.reg_no




