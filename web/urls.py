from django.conf.urls import url

from web import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^about/$', views.about),
    url(r'^faq/$', views.faq),
    url(r'^contact/$', views.contact)


]